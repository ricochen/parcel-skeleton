## Quick start

```
git archive --remote=git@gitlab.com:ricochen/parcel-skeleton.git | tar x -C /path/to/project-directory
```

or

```
curl -s https://gitlab.com/ricochen/parcel-skeleton/raw/master/init.sh | bash -s /path/to/project-directory
```

Note: change /path/to/project-directory