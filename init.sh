if [ $# -lt 1 ]; then
    echo "Usage: program /path/to/project [branch]"
    echo "Please provide the path to the project that you are creating"
    exit 1
fi
dir=$1 
[ -d $dir ] || mkdir $dir
git archive --remote=git@gitlab.com:ricochen/parcel-skeleton.git ${2:-master} | tar x -C $dir