import React from 'react'

export default class extends React.Component {
    state = {
        message: 'Hello World!'
    }

    render() {
        return <h1>
            {this.state.message}
        </h1>
    }
}